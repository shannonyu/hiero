#!/bin/bash

# Usage: extract.sh
# Assumes that a Hadoop cluster exists and HADOOP_CONF_DIR is set
# This doesn't necessarily have to be run on a node inside the cluster

# These are paths internal to Notre Dame
NLP=/afs/crc.nd.edu/group/nlp

module load java/1.7

module load python/2.7.8
PYTHON=`which python`

HIERO=$NLP/01/dchiang/hiero
export PATH=$HIERO:$HIERO/grammar-hadoop:$PATH

export HADOOP_PREFIX=$NLP/software/hadoop/1.2.1
export PATH=$HADOOP_PREFIX/bin:$PATH
. $HIERO/grammar-hadoop/hadoop-utils.sh
HADOOPSTREAM="hadoop jar $HADOOP_PREFIX/contrib/streaming/hadoop-streaming-*.jar -cmdenv PYTHONPATH=$HIERO:$HIERO/grammar-hadoop:$HIERO/cython -cmdenv LD_LIBRARY_PATH=$HIERO/cython"

# Where all the input files live
DATA=$NLP/data/elisa/hau/y1r1.v0
PREFIX=elisa.hau-eng.train.y1r1.v0
LEX_F2N=$DATA/model/lex.f2e
LEX_N2F=$DATA/model/lex.e2f

WORKDIR=.
hadoop fs -mkdir $WORKDIR

NODES=$(wc -l < $HADOOP_CONF_DIR/slaves)
MAPS_PER_NODE=10

pack-input.py $DATA/$PREFIX.{hau,eng}.clean $DATA/model/aligned.grow-diag-final-and | hadoop fs -put - ${WORKDIR}/train

##################################################################
# Rule extraction
#
# Map:    training sentence pair -> extracted (rule, count)s
# Reduce: (rule, counts) -> (rule, sum of counts)

# Hadoop will use one mapper per block (128M) of input
# but because the extractor generates so much more output than input
# (for rule extraction, about 200x; for phrase extraction, less than 10x), 
# we need to provide a better hint

hadoop_stream \
    -input $WORKDIR/train \
    -mapper "$PYTHON $HIERO/grammar-hadoop/extractor.py -L15 -l6 -A -u --english-loose-limit 2" \
    -reducer "$PYTHON $HIERO/grammar-hadoop/sumvector.py" \
    -jobconf mapred.map.tasks=$(($NODES * $MAPS_PER_NODE))

##################################################################
# Select most frequent alignment
#
# Map:    rule -> (rule without alignment, rule)
# Reduce: (rule without alignment, rules) -> 
#                select most frequent rule and sum counts

hadoop_stream \
    -mapper "$PYTHON $HIERO/grammar-hadoop/rulepart.py rule" \
    -reducer "$PYTHON $HIERO/grammar-hadoop/maxcount.py" 

##################################################################
# Conditional probabilities
# 
# Map:     rule -> (normalization group, rule)
# Reduce:  (normalization group, rule) -> 
#                new feature = count / sum of counts

hadoop_stream \
    -mapper "$PYTHON $HIERO/grammar-hadoop/rulepart.py frhs" \
    -reducer "$PYTHON $HIERO/grammar-hadoop/condprob.py pef" 

hadoop_stream \
    -mapper "$PYTHON $HIERO/grammar-hadoop/rulepart.py erhs" \
    -reducer "$PYTHON $HIERO/grammar-hadoop/condprob.py pfe"

##################################################################
# Lexical weighting and cross attribute
#
# Map:     rule -> rule with new feature

hadoop fs -put $LEX_F2N $WORKDIR/lex.f2n
hadoop fs -put $LEX_N2F $WORKDIR/lex.n2f

hadoop_stream \
    -mapper "$PYTHON $HIERO/grammar-hadoop/lexical.py lex.f2n lef" \
    -cacheFile $WORKDIR/lex.f2n\#lex.f2n

hadoop_stream \
    -mapper "$PYTHON $HIERO/grammar-hadoop/lexical.py lex.n2f lfe" \
    -cacheFile $WORKDIR/lex.n2f\#lex.n2f 

hadoop_stream -mapper "$PYTHON $HIERO/grammar-hadoop/crossing.py"

### Convert rules back to Hiero format

hadoop_stream \
    -output $WORKDIR/rules.final \
    -mapper "$PYTHON $HIERO/grammar-hadoop/finish-rule.py"

rm -rf rules.final
hadoop fs -get $WORKDIR/rules.final .
